let server = require("../app/app");
let request = require('request');
let chai = require('chai');
let assert = require('chai').assert;
let expect = require('chai').expect;
let cheerio = require('cheerio');
// don't .js extension, common js handles it perfectly
let should = chai.should();

let chaiHttp = require('chai-http');
chai.use(chaiHttp);
describe("/GET  item in subcategory", () => {
    it('should return item name when page is open', (done) => {

        chai.request(server)
            .get('/products/mens-clothing-suits/25686364')
            .end((err, res) => {
                res.should.have.status(200);
                expect(res).to.be.a('object');
                let bodyText = cheerio.load(res.text);
                let textid = bodyText('#productname').html();
                expect(textid).to.have.equal(' Charcoal Single Pleat Striped Wool Suit');
                res.body.should.be.a('object');
                done();
            });
    });
});

describe("Main page profile form", () => {
    it('profile form should not be sent if user doesnt log in, log-in and email recovery forms shold be present', (done) => {

        chai.request(server)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                expect(res).to.be.a('object');
                let bodyText = cheerio.load(res.text);
                should.not.exist(bodyText('#profile_modal').html());
                should.exist(bodyText('#login-modal').html());
                should.exist(bodyText('#email-recovery').html());
                res.body.should.be.a('object');
                done();
            });
    });
});

describe("Log in", () => {
    it('log-in with bad username - password combination', (done) => {

        chai.request(server)
            .post('/login')
            .send({
                '_method': 'POST',
                'user': '123',
                'pass': '123'
            })

            .end((err, res) => {
                //               res.should.have.status(200);
                expect(res).to.be.a('object');
                let bodyText = cheerio.load(res.text);

                expect(bodyText('#errormsg').html()).to.have.equal('\n        Bad login-password pair\n    ');
                res.body.should.be.a('object');
                done();
            });
    });

});

describe("Log in", () => {
    it('log-in with right username - password combination', (done) => {

        chai.request(server)
            .post('/login?user=user&pass=1')
            .end((err, res) => {
                res.should.have.status(200);
                expect(res).to.be.a('object');
                let bodyText = cheerio.load(res.text);
                expect(bodyText('#index-greet').html()).to.have.equal('WELCOME');
                res.body.should.be.a('object');
                done();
            });
    });

});