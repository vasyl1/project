var mongoose = require('mongoose'), //connect plugins
    Schema = mongoose.Schema;
//create schema for items in cart
var itemsSchema = new Schema({
    id: String,
    name: String,
    price: String,
    description: String,
    image: String,
    quantity: Number
});
//create schema for cart
var shoppingCartSchema = new Schema({
    items: [itemsSchema],
    userId: String,
    state: String,
    address: String,
    total: String,
    date: String
});


module.exports = mongoose.model('carts', shoppingCartSchema);