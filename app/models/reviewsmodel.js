var mongoose = require('mongoose'), //connect plugins
    Schema = mongoose.Schema;
//create schema for reviews
var reviewsSchema = new Schema({
    itemId: String,
    username: String,
    reviewTitle: String,
    reviewContent: String,
    rating: Number
});

module.exports = mongoose.model('reviews', reviewsSchema);