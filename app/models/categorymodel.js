var mongoose = require('mongoose'), //connect plugins
    Schema = mongoose.Schema;
//create schema for sub-subcategory, that is nested in subcategory document
var categoriesSubChildSchema = new Schema({
    id: String,
    image: String,
    name: String,
    page_description: String,
    page_title: String,
    parent_category_id: String,
    c_showInMenu: Boolean
});
//create schema for subcategory, that is nested in category document and have nested sub-subcategory document
var categoriesChildSchema = new Schema({
    categories: [categoriesSubChildSchema],
    id: String,
    image: String,
    name: String,
    page_description: String,
    page_title: String,
    parent_category_id: String,
    c_showInMenu: Boolean
});
//create schema for categories
var categoriesParentSchema = new Schema({
    categories: [categoriesChildSchema],
    id: String,
    name: String,
    page_description: String,
    page_title: String,
    parent_category_id: String,
    c_showInMenu: Boolean,
});


module.exports = mongoose.model('categories', categoriesParentSchema);