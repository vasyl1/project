var mongoose = require('mongoose'), //connect plugins
    Schema = mongoose.Schema;
//create schema for users
var userSchema = new Schema({
    username: String,
    first_name: String,
    last_name: String,
    email: String,
    address: String,
    password: String,
    registered: String,
    verified: Boolean

});


module.exports = mongoose.model('users', userSchema);