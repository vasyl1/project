var mongoose = require('mongoose'), //connect mongoose plugin
    Schema = mongoose.Schema;
// create schema for image data, nested in images document
var productImagesSchema = new Schema({
    alt: String,
    link: String,
    title: String
});
// create schema for images array document, nested in product document
var productImagesGroupsSchema = new Schema({
    images: [productImagesSchema],
    view_type: String,
    variation_value: String
});
// create schema for products document, that contain nested document image_groups
var productSchema = new Schema({
    id: String,
    short_description: String,
    long_description: String,
    page_title: String,
    name: String,
    price: Number,
    currency: String,
    primary_category_id: String,
    image_groups: [productImagesGroupsSchema]
});


module.exports = mongoose.model('products', productSchema);