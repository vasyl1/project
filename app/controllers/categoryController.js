//connect to model
var Categories = require('../models/categorymodel.js');

//get all categories names and ids for navigation bar buttons
Categories.find({}, {
    name: true,
    id: true,
    _id: false
}).exec(function(err, cat) {
    if (err) {
        throw err;
    }
    // Successful, so render
    categoryButtons = cat;
});

// get data for index page
exports.list = function(req, res, next) {
    res.render('index', {
        pageTitle: "Main page",
        pageID: "index",
        user: req.user
    });
};

// get subcategory data for chosen category
exports.subList = function(req, res, next) {
    // with mongoose script get subcategory with id, that we receive in router
    Categories.find({
        'id': req.params.categoryId
    }, {
        _id: false
    }).exec(function(err, cat) {
        if (err) {
            throw err;
        }
        // render categories view and pass subcategory data to display
        res.render('categories', {
            allCategories: cat,
            pageTitle: cat[0].page_title,
            pageID: cat[0].id,
            user: req.user
        });
    });
};

// get subcategory data for chosen subcategory
exports.subCategoryList = function(req, res, next) {
    // with mongoose script get subcategory for subcategory with id and parent_category_id, that we receive in router    
    Categories.find({
        "categories.categories.parent_category_id": req.params.subCategoryId,
        "id": req.params.categoryId
    }, {
        "categories.$": true
    }).exec(function(err, cat) {
        if (err) {
            throw err;
        }
        //render subcategoies view and pass sub-subcategory data to display
        res.render('subcategories', {
            allCategories: cat[0].categories[0],
            pageTitle: cat[0].categories[0].page_title,
            pageID: cat[0].categories[0].id,
            user: req.user
        });
    });
};