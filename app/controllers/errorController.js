//error page
exports.display = function(req, res, next) {
    res.render('error', {
        Message: "Error",
        pageTitle: "Error",
        pageID: "error"
    });
}