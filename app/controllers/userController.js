var users = require('../models/usermodel.js');
var cart = require('../models/cartmodel.js');
var bcrypt = require('bcrypt');
var express = require('express');
var sendEmail = require('../configuration/email.js');
var app = express();
//setup passport
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
//setup strategy for local sign-in
passport.use(new LocalStrategy({
    usernameField: 'user',
    passwordField: 'pass'
}, function(username, password, done) {
    users.findOne({
        "username": username
    }, function(err, user) {
        //check if username exist
        if (!user) {
            return done(null, false, {
                message: 'Incorrect username.'
            });
        }
        //check if passwords match
        if (!bcrypt.compareSync(password, user.password)) {
            return done(null, false, {
                message: 'Incorrect password.'
            });
        }
        //if everything fine, return user variable as req.user
        return done(null, user);
    });
}));

//if we hit registration
exports.register = function(req, res, next) {
    //check if its first time (no data is entered)
    if (Object.keys(req.query).length === 0) {
        res.render('userregister', {
            pageTitle: "Register new user",
            pageID: "register",
            message: ''
        });
    } else {
        //check if all fields are filled
        if (req.query.username == "" || req.query.first_name == "" || req.query.email == "" || req.query.password == "") {
            res.render('userregister', {
                pageTitle: "Register new user",
                pageID: "register",
                message: 'Fill all fields'
            });
        } else {
            //check if there is username with same name
            users.find({
                username: req.query.username
            }).exec(function(err, user) {
                if (Object.keys(user).length != 0) {
                    res.render('userregister', {
                        pageTitle: "Register new user",
                        pageID: "register",
                        message: 'Choose another username'
                    });
                } else {
                    //if everything ok, save user in database
                    var user = new users({
                        username: req.query.username,
                        first_name: req.query.first_name,
                        last_name: req.query.last_name,
                        email: req.query.email,
                        registered: new Date().toJSON().slice(0, 10),
                        password: bcrypt.hashSync(req.query.password, 10),
                        verified: false
                    });
                    user.save();
                    var newCart = new cart({
                        "userId": req.query.username,
                        "state": "cart"
                    });
                    newCart.save();
                    res.redirect('/');
                }
            });
        }
    }
}

// sign-in with passport local strategy, if some error occured - redirect at error page, if login and password match - redirect at index page
exports.login = passport.authenticate('local', {
    successRedirect: 'back',
    failWithError: true
});

exports.errorLogin=function(err, req, res, next) {
    // handle error
    return res.render('error', {
                "Message": "Bad login-password pair",
                pageTitle: "Error",
                pageID: "error"
            });
  }
//end session if user hit logout button
exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
};

// method for password recovery
exports.passwordRecovery = function(req, res) {
    // first check db collection for email from query
    users.findOne({
        email: req.query.email
    }, function(err, result) {
        //if there is no such email
        if (Object.keys(result).length == 0) {
            res.render('error', {
                "Message": "There is no such email",
                pageTitle: "Error",
                pageID: "error"
            });
        } else {
            // generate new password
            var newPassword = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 8);
            //and encypt it
            result.password = bcrypt.hashSync(newPassword, 10);
            result.save();
            //and send it via email
            sendEmail(req.query.email, 'Password recovery', 'Your new password: ' + newPassword);
            res.redirect('/');
        }
    })
};

// method for email verification
exports.verifyEmail = function(req, res) {
    //check query
    if (typeof(req.query.email) != "undefined") {
        //if user hit "verify" - we send him email
        if (typeof(req.query.verify) == "undefined") {
            //generate link for verification
            var link = "http://" + req.get('host') + "/verify/?email=" + req.query.email + "&verify=" + bcrypt.hashSync(req.query.email, 10);
            //and send email
            sendEmail(req.query.email, 'Email verification', `<a href="` + link + `">Click this to verify your email</a>`);
        }
        //if user hit verify link from email
        else {
            //check verify string
            if (bcrypt.compareSync(req.query.email, req.query.verify) == true) {
                //find in collection user with that email
                users.findOne({
                    email: req.query.email
                }, function(err, result) {
                    //and set value of variable verified to true
                    result.verified = "true";
                    result.save();
                });
            }
        }
    }
    res.redirect('/');
};