var cart = require('../models/cartmodel.js');
var userModel = require('../models/usermodel.js');
var product = require('../models/productmodel.js');
var sendEmail = require('../configuration/email.js');

//add items to wishlist or cart
exports.addToCart = function(req, res, next) {
    var user = req.query.user;
    var product_id = req.query.product_id;
    var found = false,
        type;

    //check what option user choose: wishlist or cart.
    ((req.query.type == "cart") || (req.query.action == "wishtocart")) ? type = "cart": type = "wishlist";
    //search data in cart collection(if that user with wishlist or cart exist)
    cart.findOne({
        userId: user,
        state: type
    }, function(error, result) {
        //get data about item, that we want to add
        product.findOne({
            id: product_id
        }, function(error, itemData) {
            var image;
            //if we want add item to wishlist - send email to user
            if (type == 'wishlist') {
                userModel.findOne({
                    username: user
                }, function(error, userData) {
                    sendEmail(userData.email, 'Wishlist', 'Item ' + itemData.name + ' has been added to your wislist');
                })
            }
            //setting up images for out item
            var n=3;
            if (itemData.image_groups[n] != undefined) {
                image = itemData.image_groups[n].images[0].link;
            } else {
                image = itemData.image_groups[0].images[0].link;
            }
            //check if item is already in wishlist or cart
            if (result != null) {
                result.items.forEach(function(item) {
                    if (item.id === product_id) {
                        item.quantity++;
                        found = true;
                    }
                });
                //if there are no such item - add data to subdocument items and send data to collection
                if (!found) {
                    result.items.push({
                        id: product_id,
                        name: itemData.name,
                        price: itemData.price,
                        image: image,
                        description: itemData.short_description,
                        quantity: 1
                    });
                }
                result.save();
            } else {
                //if user cart is empty - create user cart and push item in it
                var newCart = new cart({
                    "userId": user,
                    "state": type
                });
                newCart.items.push({
                    id: product_id,
                    name: itemData.name,
                    price: itemData.price,
                    image: image,
                    description: itemData.short_description,
                    quantity: 1
                });
                newCart.save();
            }
        });
    });
    //and redirect to page, that we want to see
    // if we choose option in wishlist (move to cart), that method will delete item document from wishlist
    if (req.query.action == "wishtocart") {
        module.exports.deleteFromWish(req, res);
    } else {
        res.redirect(`/${type}`);
        next();
    }
}

//method to display cart items
exports.showCart = function(req, res, next) {
    //variable, that will store total cost of shopping cart
    var total = 0;
    //display if error
    if (typeof(req.user) === "undefined") {
        res.render('error', {
            "Message": "Cart is empty",
            pageTitle: "Error",
            pageID: "error"
        });
    } else {
        //find user data in cart collection
        cart.find({
            "userId": req.user[0].username,
            state: "cart"
        }, function(error, result) {
            if (result === []) {
                res.render('error', {
                    "Message": "Cart is empty",
                    pageTitle: "Error",
                    pageID: "error"
                });
            } else {
                //calculate total price of cart
                result[0].items.forEach(function(item) {
                    total += parseInt(item.price);
                });
                //and pass that data to view
                res.render("cart", {
                    total: total,
                    cartList: result[0].items,
                    pageID: "shoppingcart",
                    pageTitle: "Shopping cart",
                    user: req.user
                });
            }
        });

    }
};

//method to display wishlist
exports.showWishlist = function(req, res, next) {
    if (typeof(req.user) === "undefined") {
        res.render('error', {
            "Message": "Wishlist is empty",
            pageTitle: "Error",
            pageID: "error"
        });
    } else {
        cart.find({
            "userId": req.user[0].username,
            state: "wishlist"
        }, function(error, result) {
            res.render("wishlist", {
                wishList: result[0].items,
                pageID: "wishlist",
                pageTitle: "Wishlist",
                user: req.user
            });
        });
    }
};

//method to display orders
exports.showOrders = function(req, res, next) {
    if (typeof(req.user) === "undefined") {
        res.redirect('/error');
    } else {
        cart.find({
            "userId": req.user[0].username,
            state: "order"
        }, function(error, result) {
            if (Object.keys(result).length === 0) {
                res.render('error', {
                    "Message": "User didnt purchase anything yet",
                    pageTitle: "Orders",
                    pageID: "orders"
                });
            } else {
                res.render("orders", {
                    orders: result,
                    pageID: "orders",
                    pageTitle: "Orders",
                    user: req.user
                });
            }
        });
    }
};

//method, that delete item from cart
exports.deleteFromCart = function(req, res, next) {
    //get all items, that user have in cart 
    cart.findOne({
        userId: req.query.user,
        state: "cart"
    }, function(error, result) {
        result.items.forEach(function(element, index) {
            //and search item, that we want to delete
            if (element.id == req.query.product_id) {
                //and delete it from subdocument
                result.items.splice(index, 1);
            }
        });
        result.save();
    });
    res.redirect('back');
};

//delete items from wishlist
exports.deleteFromWish = function(req, res, next) {
    cart.findOne({
        userId: req.query.user,
        state: "wishlist"
    }, function(error, result) {
        result.items.forEach(function(element, index) {
            if (element.id == req.query.product_id) {
                result.items.splice(index, 1);
            }
        });
        result.save();
    });
    res.redirect("/wishlist");
};

//method, that will get count of items in cart and wishlist
exports.getDataForProfile = function(req, res, next) {
    if (typeof(req.user) != "undefined") {
        var promise = new Promise(function(resolve, reject) {
            //get from db two rows - data for cart list and data for wishlist with number of items in subdocuments
            cart.aggregate([{
                    $match: {
                        "userId": req.user[0].username,
                        $or: [{
                            "state": "cart"
                        }, {
                            state: "wishlist"
                        }]
                    }
                },
                {
                    $project: {
                        numberOfItems: {
                            $size: "$items"
                        },
                        state: 1
                    }
                }
            ],
            function(err, result) {
                resolve(result);
            });
        });
        promise.then(function(result) {
            req.app.locals.listItems = result;
            next();
        });
    } else {
        next();
    }
}

//method, that performs actions, when user hit "Buy now" button in cart
exports.total = function(req, res, next) {
    if (typeof(req.user) == "undefined") {
        res.render('error', {
            "Message": "Please log in",
            pageTitle: "Error",
            pageID: "error"
        });
    } else {
        //find cart element in collection for this user
        cart.findOne({
            userId: req.user[0].username,
            state: "cart"
        }, function(error, result) {
            //if cart empty - display that
            if (result == {}) {
                res.render('error', {
                    "Message": "Your cart is empty",
                    pageTitle: "Error",
                    pageID: "error"
                });
            } else {
                var total = 0;
                var stringForEmail = "Your order: ";
                //calculate total cost and string, that contain item name for email.
                result.items.forEach(function(item) {
                    total += Number(item.price);
                    stringForEmail += item.name + ", ";
                });
                //add date, total, and state as order db fields 
                result.date = new Date().toJSON().slice(0, 10);
                result.total = total;
                result.state = "order";
                result.save();
                //we "delete" old cart (it serves as order), so create new one
                var newCart = new cart({
                    "userId": req.user[0].username,
                    "state": "cart"
                });
                newCart.save();
                //and send email
                stringForEmail += "total price: " + total + "USD. \n Date - " + result.date;
                sendEmail(req.user[0].email, 'Your order:', stringForEmail);
            res.render('error', {
            "Message": "Thank you for purchasing on our website, you can find details of order on the email",
            pageTitle: "Thank You",
            pageID: "successPurchase",
            user:req.user
        });
            }
        });
    }
}