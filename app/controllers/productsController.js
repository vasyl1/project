//connect to model
var products = require('../models/productmodel.js');
var reviews = require('../models/reviewsmodel.js');
var soap = require('soap');
//SOAP model URL
var config = require('../configuration/config.js');
var URL = config.get('SOAP');
var CURRENCY = [];
var reviewsForItem = [];
// get data for products list in sub-subcategory
exports.productsList = function(req, res, next) {
    //create query parameters
    var QUERY = {};
    var MESSAGE;
    //check: if user hit "items in category" - display that items
    if (req.params.subCategoryId != null) {
        QUERY = {
            "primary_category_id": req.params.subCategoryId
        };
    }
    // if user choose search bar - display items
    else {
        //get string from search bar and search name with that string
        QUERY = {
            "name": {
                "$regex": req.params.requestItem,
                "$options": "i"
            }
        }
        //create header for our result page
        MESSAGE = "Found for '" + req.params.requestItem + "' items:";
    };
    //get products in subcategory, that we get in router
    products.find(QUERY).exec(function(err, productsInCategory) {
        // if search has no results
        if (JSON.stringify(productsInCategory) === '[]') {
            res.render('error', {
                "Message": "No items in this cagerory",
                pageTitle: "Error",
                pageID: "error",
                user: req.user
            });
        } else {
            // every product data contain different image data, we need medium size images for every item
            // so we loop every image_groups item for every product and search for "medium" and save his index
            // for example for 3rd product medium image would be in image_groups array with index mediumImageIndex[3] and so on
            var mediumImageIndex = [];
            productsInCategory.forEach(function(el, elIndex) {
                el.image_groups.forEach(function(img_group, index) {
                    if (img_group.view_type == "medium" && img_group.variation_value == null) {
                        mediumImageIndex[elIndex] = index;
                    }
                });
            });
            //render products view and pass data to display
            res.render('products', {
                allProducts: productsInCategory,
                mediumImageIndex,
                pageTitle: "Products list",
                pageID: "products",
                MESSAGE,
                user: req.user
            });
        }
    });
};
// get data for single product item
exports.singleProduct = function(req, res, next) {
    // find item to display by id and primary_category_id, that we get from router
    products.find({
        "primary_category_id": req.params.subCategoryId,
        "id": req.params.productId
    }).exec(function(err, singleProductItem) {
        var promiseReview = new Promise(function(resolve, reject) {
            //get data for reviews
            reviews.find({
                itemId: singleProductItem[0].id
            }).exec(function(err, reviewList) {
                resolve(reviewList);
            });
        });
        promiseReview.then(function(review) {
            reviewsForItem = review;
        });
        // find big images for product like we did with meduim
        var bigImageData = [];
        singleProductItem[0].image_groups.forEach(function(img_group, index) {
            if (img_group.view_type == "large" && img_group.variation_value == null) {
                bigImageData = img_group.images;
            }
        });

        //creating SOAP client
        soap.createClient(URL, function(err, client) {
            //get data from SOAP service
            var promise = new Promise(function(resolve, reject) {
                //first we get last date, when the currency cource was updated with method lastdateinserted
                client.lastdateinserted(function(err, res) {
                    //pass result to next method, that will display all courency cource for some date
                    resolve(res.lastdateinsertedResult);
                });
            });

            // getting all pairs Currency:Value
            promise.then(function(dateFromQuery) {
                client.getall({
                    dt: dateFromQuery.toJSON().slice(0, 10)
                }, function(err, res) {

                    //currency list - new currency can be add as {cur:"ITM",value:0}
                    CURRENCY = [{
                        cur: "USD",
                        value: 0
                    }, {
                        cur: "UAH",
                        value: 0
                    }, {
                        cur: "EUR",
                        value: 0
                    }, {
                        cur: "GBP",
                        value: 0
                    }];
                    //in resulting object searching for currencies, that we want - in CURRENCY object(United States Dollar, Ukrainian Hryvnya, Euro)

                    res.getallResult.diffgram.DocumentElement.Currency.forEach(function(responseData) {
                        CURRENCY.forEach(function(curElement) {
                            // if we find match, change element value
                            if (responseData.IDMoneda === curElement.cur) {
                                curElement.value = responseData.Value;
                            }
                        });
                    });
                    // pass data to next function in chain
                    return (CURRENCY);
                    next();
                });
            }).then(
                //render single product view and pass data to display
                res.render('product', {
                    CURRENCY,
                    productData: singleProductItem[0],
                    bigImageData: bigImageData[0],
                    headButton: singleProductItem,
                    pageTitle: "Product",
                    pageID: "product",
                    user: req.user,
                    reviewsForItem
                }));
        });
    });
};

//method, that will add new review for item
exports.addReview = function(req, res, next) {
    if (typeof(req.user) != "undefined") {
        //get data from query and for new collection field
        var review = new reviews({
            itemId: req.query.item_id,
            username: req.user[0].first_name + " " + req.user[0].last_name,
            reviewTitle: req.query.reviewTitle,
            reviewContent: req.query.reviewContent,
            rating: req.query.rating
        });
        review.save();
        res.redirect('back');
    } else
        res.redirect('back');
}