/**
 * File with configuration data
 */
var convict = require('convict');

// Define a schema
var config = convict({
    env: {
        doc: "The application environment.",
        format: ["production", "development", "test"],
        default: "development",
        env: "NODE_ENV"
    },
    ip: {
        doc: "The IP address to bind.",
        format: "ipaddress",
        default: "127.0.0.1",
        env: "IP_ADDRESS",
    },
    port: {
        doc: "The port to bind.",
        format: "port",
        default: 3000,
        env: "PORT",
        arg: "port"
    },
    db: {
        host: {
            doc: "Database host",
            default: 'mongodb://localhost:27017',
            env: "DB_HOST"
        },
        name: {
            doc: "Database name",
            format: String,
            default: 'shop'
        }
    },
    SOAP: {
        doc: "SOAP web service address",
        default: "http://infovalutar.ro/curs.asmx?wsdl"
    },
    email: {
        service: "Gmail",
        user: "sendmailosf@gmail.com",
        pass: "123sendmail"
    }
});

// Load environment dependent configuration
var env = config.get('env');
// Perform validation
config.validate();

module.exports = config;