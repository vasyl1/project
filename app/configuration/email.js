var nodemailer = require('nodemailer');
var config = require('./config.js');
//configurate email sending
const transport = nodemailer.createTransport({
    service: config.get('email').service,
    auth: {
        //fill email adress and password fields
        user: config.get('email').user,
        pass: config.get('email').pass,
    },
    tls: {
        rejectUnauthorized: false
    }
});
module.exports = function sendEmail(to, subject, message) {
    const mailOptions = {
        from: config.get('email').user,
        to,
        subject,
        html: message,
    };
    transport.sendMail(mailOptions, (error) => {
        if (error) {
            message(error)
        }
    });
};