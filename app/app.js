var express = require('express'); //connecting plugins
var reload = require('reload');
var app = express();
var config = require('./configuration/config.js');
var users = require('./models/usermodel.js');
var mongoose = require("mongoose"); //connecting to mongoose
mongoose.connect(config.get('db').host + "/" + config.get('db').name); //establish db connection with value, that we get from configuration file
var session = require("express-session"),
    bodyParser = require("body-parser");
//setup password module
var passport = require("passport");
app.use(express.static("public"));
app.use(session({
    secret: 'scr',
    resave: true,
    saveUninitialized: true
}));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(passport.initialize());
//start session
app.use(passport.session());
//define how user will be pull-in and pull-out in session
passport.serializeUser(function(user, done) {
    done(null, user.username);
});
passport.deserializeUser(function(id, done) {
    users.find({
        "username": id
    }, function(err, user) {
        done(err, user);
    });
});
//Setting up variables for server, view engine and location of application views
app.set('port', process.env.PORT || config.get('port'));
app.set('view engine', 'ejs');
app.set('views', 'app/views');
app.locals.siteTitle = 'OSF SHOP';

//setup directory with static data
app.use(express.static('app/public'));
//connecting routes, which are called depending on the request
app.use(require('./routes/index'));
//starting server
let server = app.listen(app.get('port'), function() {});
//if some changes were made on the server, reload plugin will reload server
reload(app);

module.exports = server