function search() {
    var inputValue = document.getElementById("mySearch").value;;
    if (inputValue != "") {
        window.location = "/search/" + inputValue;
    }
}

jQuery(document).ready(function() {
    $("#input-21f").rating({
        starCaptions: function(val) {
            return val;
        },
        starCaptionClasses: function(val) {
            if (val < 3) {
                return 'label label-danger';
            } else {
                return 'label label-success';
            }
        },
        hoverOnClear: false
    });

    $('.input-3').rating({
        displayOnly: true,
        step: 0.5
    });
});