//connect plugins and category controller
var express = require('express');
var router = express.Router();
var cart = require("../controllers/cartController");
//add to cart and wishlist
router.get('/addtocart', cart.addToCart);
//remove item from cart
router.get('/deletefromcart', cart.deleteFromCart);
//remove item from wishlist
router.get('/deletefromwishlist', cart.deleteFromWish);
//show cart items
router.get('/cart', cart.showCart);
//show wishlist items
router.get('/wishlist', cart.showWishlist);
router.get('/orders', cart.showOrders);
router.get('/total', cart.total);

module.exports = router;