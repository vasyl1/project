//connect plugins and category controller
var express = require('express');
var router = express.Router();
var user = require("../controllers/userController");

router.get('/register', user.register);
router.post('/login', user.login,user.errorLogin);
router.get('/logout', user.logout);
router.get('/recovery', user.passwordRecovery);
router.get('/verify', user.verifyEmail);
module.exports = router;