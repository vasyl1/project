//connect plugins and category controller
var express = require('express');
var router = express.Router();
var categories = require("../controllers/categoryController");
var cart = require("../controllers/cartController");
var error = require("../controllers/errorController");

router.get('/*', cart.getDataForProfile);
//redirect to controller, that gather data for index page
router.get('/', categories.list);
//redirect to controller, that gather data for category
router.get('/category/:categoryId', categories.subList);
//redirect to controller, that gather data for subcategory  
router.get('/category/:categoryId/:subCategoryId', categories.subCategoryList);
router.use('/error', error.display);
router.use(require('./products'));
router.use(require('./user'));
router.use(require('./cart'));

module.exports = router;