//connect plugins and products controller
var express = require('express');
var router = express.Router();
var products = require("../controllers/productsController");

//redirect to controller, that gather data for product list page
router.get('/products/:subCategoryId', products.productsList);
router.get('/search/:requestItem', products.productsList);
//redirect to controller, that gather data for single product page
//router.get('/products/:subCategoryId/:productId', products.displayReviewData);
router.get('/products/:subCategoryId/:productId', products.singleProduct);
router.get('/addreview', products.addReview);
module.exports = router;